#include "BlackJack.h"

//Entry point for the Blackjack application.
//Initializes the BlackJack object and then runs its internal logic.
int main()
{
	BlackJack blackJackGame = BlackJack();
	blackJackGame.run();
}
