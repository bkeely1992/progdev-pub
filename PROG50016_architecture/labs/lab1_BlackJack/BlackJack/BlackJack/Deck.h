﻿#pragma once
#include <list>
#include "Card.h"
#include <iostream>
#include <vector>
#include <chrono>
#include <random>
#include <algorithm>

using namespace std;

//Implementation of a deck of cards. Can contain multiple 52 card packs in a single deck.
class Deck
{
	//The four types of suits.
	list<string> SUITS = { "H", "D", "S", "C"};
	vector<Card> cards;
	

public:
	Deck() {}
	Deck(int numberOfDecks);
	Card dealCard();
	int getCardsLeftCount();
};

