#pragma once
#include "Card.h"

class Hand
{
	vector<Card> cards;

public:
	void addCardToHand(Card newCard);
	int getCurrentBlackjackValue();
	void clearHand();
	string toString();
	int getSize();
};

