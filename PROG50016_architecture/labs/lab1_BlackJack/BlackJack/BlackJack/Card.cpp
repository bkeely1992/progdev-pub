#include "Card.h"

//Provided the id for the type of card and a suit
//Sets the internal values of the card class.
void Card::setCardInfo(int inID, string inSuit) 
{
	Value = -1;

	//If the value is less than ten, then it isn't a face card and the value just defaults to the ID value.
	if (inID < 11) {
		Value = inID;
	}
	else {
		Value = 10;
	}

	//Uses a dictionary of the card names indexed with the ID value to get the value of the card and set it.
	//Throws an error if an incorrect ID is provided.
	if (CARDNAMES.find(inID) == CARDNAMES.end()) {
		throw new exception("Invalid card ID was entered. Exiting.");
	}
	name = CARDNAMES[inID];

	FullName = name + suit;
}

Card::Card() {
	Value = -1;
	FullName = "";
}

Card::Card(string inSuit, int inID) {
	id = inID;
	suit = inSuit;
	setCardInfo(id, suit);
	IsFlipped = false;
}