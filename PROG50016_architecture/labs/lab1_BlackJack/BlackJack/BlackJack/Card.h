#pragma once
#include <string>
#include <algorithm>
#include <unordered_map>
using namespace std;

//Class used to hold the internal details of a card.
class Card
{
	string name;
	string suit;
	int id;

	//Dictionary used to store the names of all of the cards in the deck.
	unordered_map<int, string> CARDNAMES =
	{
		{1, "A"}, {2, "2"}, {3, "3"}, {4, "4"}, {5, "5"}, {6, "6"}, {7, "7"},
		{8, "8"}, {9, "9"}, {10, "10"}, {11, "J"}, {12, "Q"}, {13, "K"}
	};

	void setCardInfo(int inID, string inSuit);

public: 
	//The name of the card.
	std::string FullName;
	int Value;

	//True if the card is flipped over. False if it is face down.
	bool IsFlipped;

	Card();
	Card(string inSuit, int inID);
};

