#include "BlackJack.h"
#include <iomanip>

//Method that handles the logic external from the game. IE: Asking the player if they want to play again and running the game loop. 
void BlackJack::run()
{
	std::cout << "==============================================================\n";
	std::cout << "                      WELCOME TO BLACKJACK!!!                 \n";
	std::cout << "==============================================================\n";
	deck = Deck(NUM_DECKS);
	string userInput = "";

	//Allows the user to exit out of the program via text input.
	while (userInput != "q")
	{
		playRound();
		std::cout << "\nPress q to quit. Otherwise any other input will start another game.\n";
		std::cin >> userInput;

		//Converts input to lower case to cover more cases.
		transform(userInput.begin(), userInput.end(), userInput.begin(), ::tolower);
	}
}

//Runs the general flow of a round of BlackJack.
void BlackJack::playRound()
{
	string userInput;
	double playerBet = 0;
	bool validBetMade = false;
	Card dealerFlippedCard;
	Card dealerHiddenCard;
	int dealerValue;
	Hand dealerHand;

	//Re-initializes the deck if the number of cards in the deck has hit the shuffle limit.
	if (deck.getCardsLeftCount() < CARDS_LEFT_SHUFFLE_LIMIT)
	{
		deck = Deck(NUM_DECKS);
	}

	//Resets the cards for the player and then gets the bet.
	currentPlayer.clearCards();
	playerBet = getPlayerBet();
	currentPlayer.Money -= playerBet;

	//Cards are dealt to player and dealer.
	dealerHand = Hand();
	dealerFlippedCard = deck.dealCard();
	dealerHiddenCard = deck.dealCard();
	dealerHand.addCardToHand(dealerFlippedCard);
	dealerHand.addCardToHand(dealerHiddenCard);
	currentPlayer.receiveCard(deck.dealCard());
	currentPlayer.receiveCard(deck.dealCard());

	std::cout << "The dealer's flipped card is: " + dealerFlippedCard.FullName + "\n";
	std::cout << "Your current hand is: " << currentPlayer.toString() + "\n";

	//Player's play is resolved.
	handlePlayerPlay(dealerHand, playerBet);

	//Dealer's play is resolved.
	dealerValue = dealerHand.getCurrentBlackjackValue();
	std::cout << "The dealer's hand is: " + dealerHand.toString() + "\n";
	dealerHand = handleDealerPlay(dealerValue, dealerHand);
	dealerValue = dealerHand.getCurrentBlackjackValue();

	//Resolves the outcome of the game, assuming
	handleGameResolution(dealerValue, playerBet, dealerHand);
}

//Prompts the user for their bet and then ensures that it is a valid bet before returning the bet value.
double BlackJack::getPlayerBet()
{
	double playerBet;
	string userInput;
	bool validBetMade = false;

	while (!validBetMade)
	{
		//Player places bet.
		std::cout << "You have $" << fixed << setprecision(2) << currentPlayer.Money << " available for betting.";
		std::cout << "Please enter your bet:\n";
		std::cin >> userInput;

		//Tries to convert the bet to a number, reprompting the user for a bet if it isn't a numerical value.
		try
		{
			playerBet = ::atof(userInput.c_str());
		}
		catch (exception e)
		{
			std::cout << "Your bet was not a valid money value.";
			continue;
		}

		//Checks if the player's bet is below the minimum bet amount. [and/or negative/zero]
		if (MIN_BET > playerBet)
		{
			std::cout << "Your bet was too low. $2.00 is the minimum bet.";
			continue;
		}
		//Checks if the player's bet is above the maximum bet amount.
		//We don't want them to bankrupt the house yknow.
		else if (MAX_BET < playerBet)
		{
			std::cout << "Your bet was too high. $500.00 is the maximum bet.";
			continue;
		}
		//Checks if the player's bet is higher than what they can even afford.
		else if (currentPlayer.Money < playerBet)
		{
			std::cout << "Your bet was too high. You only have $" + to_string(currentPlayer.Money) + " available for betting.";
			continue;
		}
		validBetMade = true;
	}

	//Once the loop has been exited from, a valid bet has been made and the method can return it.
	return playerBet;
}

//Interacts with the player and manages their turn.
void BlackJack::handlePlayerPlay(Hand dealerHand, double playerBet)
{
	string userInput;
	bool noValidInputReceived;

	//Returns if the player has a natural. [ie: there's no need for the player to do anything]
	if (currentPlayer.HasNatural)
	{
		return;
	}

	userInput = "";

	//Loop the player interactions until they bust, get a natural or want to stay.
	while (!currentPlayer.IsBusted && userInput != "stay" && !currentPlayer.HasNatural)
	{
		std::cout << "Do you want to hit or stay?:\n";
		noValidInputReceived = true;

		//Re-iterates until a valid input is received.
		while (noValidInputReceived)
		{
			std::cin >> userInput;

			//Player receives a card if they ask for one.
			if (userInput == "hit")
			{
				currentPlayer.receiveCard(deck.dealCard());
				noValidInputReceived = false;
			}
			//Player exits out of the interaction loop if they want to stay.
			else if (userInput == "stay")
			{
				break;
			}
			//Handling for an incorrect input.
			else
			{
				std::cout << "Invalid input, please try again.\n";
			}
		}
		std::cout << "Your current hand is: " << currentPlayer.toString() + "\n";
	}

	
}

//Handles the operating procedures of the dealer's play.
//Returns the dealer's hand to compare against the player's hand.
Hand BlackJack::handleDealerPlay(int dealerValue, Hand dealerHand)
{
	//If the player is busted or has a natural, the dealer does not have to do anything and this method returns.
	if (currentPlayer.IsBusted || currentPlayer.HasNatural)
	{
		return dealerHand;
	}

	//As long as the dealer's hand is lower than the stop limit, they'll continue to add cards to their hand.
	while (dealerValue < DEALER_STOP_LIMIT)
	{
		dealerHand.addCardToHand(deck.dealCard());
		dealerValue = dealerHand.getCurrentBlackjackValue();
		std::cout << "The dealer's hand is: " + dealerHand.toString() + "\n";
	}

	return dealerHand;
}

//Handles the operating procedures for completing the current round of Blackjack.
//Pays out to the user where applicable.
void BlackJack::handleGameResolution(int dealerValue, double playerBet, Hand dealerHand)
{
	//Handling for when the player specifically has a natural.
	if (currentPlayer.HasNatural) 
	{
		std::cout << "You have a natural!\n";

		//If the dealer does not also have a natural, pays out.
		if (dealerHand.getCurrentBlackjackValue() != 21)
		{
			std::cout << "Dealer's hand is not also a natural, you win!\n";
			currentPlayer.Money += playerBet * 2.5;
		}
		//If the dealer also has a natural then it's a tie.
		else
		{
			std::cout << "Dealer's hand is also a natural. Returning bet.\n";
			currentPlayer.Money += playerBet * 1.0;
		}

		currentPlayer.HasNatural = false;
		return;
	}
	//If the player's hand is busted, then they exit the game without any winnings immediately.
	else if (currentPlayer.IsBusted)
	{
		std::cout << "You went over 21!\n";
		currentPlayer.IsBusted = false;
		handleLoseResolution();

		return;
	}
	//If the dealer's hand is busted, then the player gets their winnings.
	else if (dealerValue > 21) {
		std::cout << "The dealer busted! You win!\n";
		currentPlayer.Money += playerBet * 2;

		return;
	}
	//If the player's hand is worse than the dealer's, then they exit the game without any winnings immediately.
	else if (dealerHand.getCurrentBlackjackValue() > currentPlayer.getCurrentBlackjackValue())
	{
		std::cout << "The dealer's hand is better than your hand!\n";
		handleLoseResolution();
		return;
	}
	//If the player's hand is equal to the dealer's hand, then their bet is returned and they exit the game.
	else if (dealerHand.getCurrentBlackjackValue() == currentPlayer.getCurrentBlackjackValue())
	{
		std::cout << "The dealer's hand is equal to your hand.\n";
		currentPlayer.Money += playerBet;
		return;
	}
	//If the player's hand is better than the dealer's, then the player gets their winnings.
	else
	{
		int test = dealerHand.getCurrentBlackjackValue();
		std::cout << "The dealer has a worse hand than you. You win!\n";
		currentPlayer.Money += playerBet * 2;
		return;
	}
}

//Handles the outcome of the player losing.
//Mainly to ensure they have money to continue playing if they run out of money.
void BlackJack::handleLoseResolution()
{
	//Resets money if it falls below the minimum amount that you need to make a bet.
	if (currentPlayer.Money < MIN_BET)
	{
		std::cout << "You no longer have enough money. You lose! Resetting your money.\n";
		currentPlayer.Money = STARTING_MONEY;
	}
}