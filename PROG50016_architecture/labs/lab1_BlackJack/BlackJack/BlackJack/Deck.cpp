#include "Deck.h"

Deck::Deck(int numberOfDecks) {

	cards = vector<Card>();

	//Given a number of card packs to add to the main deck, adds that many to the deck.
	for (int i = 0; i < numberOfDecks; i++)
	{
		//Iterates over the four suits to add a card value for each of those suits for every card pack.
		for (string suit : SUITS)
		{

			//Iterates from 1-13 to add those card IDs to the deck for every suit for every card pack.
			for (int cardNumber = 1; cardNumber < 14; cardNumber++) {
				cards.push_back(Card(suit, cardNumber));
			}
		}
	}

	//Shuffles the cards so that they're in a randomized order.
	srand(unsigned(time(0)));
	random_shuffle(cards.begin(), cards.end());
}

//Takes the card off the front of the deck, removes it from the deck and returns it.
Card Deck::dealCard()
{
	Card topCard = cards.front();
	cards.erase(cards.begin());
	return topCard;
}

//Returns the number of cards left in the deck.
int Deck::getCardsLeftCount()
{
	return cards.size();
}

//In a more rigorous class I would add a return card method, but this implementation doesn't need it.


