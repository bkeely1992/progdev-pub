#pragma once
#include <string>
#include "Hand.h"
#include "Card.h"

class Player
{
	Hand hand;
	int currentValue = 0;

public:
	double Money = 50.00;
	bool IsBusted = false;
	bool HasNatural = false;

	void receiveCard(Card newCard);
	void clearCards();
	string toString();
	int getCurrentBlackjackValue();
};

