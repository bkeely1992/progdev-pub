#pragma once
#include <iostream>
#include <string>
#include "Deck.h"
#include "Player.h"
#include "Hand.h"

//Rules taken from: https://bicyclecards.com/how-to-play/blackjack/
//Implementation of the general flow of the Blackjack program.
//Manages all of the user interaction.
class BlackJack 
{
	//The number of decks shuffled into the Blackjack deck at the start of execution.
	const int NUM_DECKS = 6;

	//When there's less than this number of cards left in the Blackjack deck, the whole thing is reshuffled.
	const int CARDS_LEFT_SHUFFLE_LIMIT = 75;

	//The value that the dealer will stop drawing cards at.
	const int DEALER_STOP_LIMIT = 17;

	//The amount of money that a player starts with.
	const double STARTING_MONEY = 50.00;

	//The minimum bet that a player must make every round.
	const double MIN_BET = 2.00;

	//The maximum bet that a player can make in a given round.
	const double MAX_BET = 500.00;

	Player currentPlayer;
	Deck deck;

	double getPlayerBet();
	void handlePlayerPlay(Hand dealerHand, double playerBet);
	Hand handleDealerPlay(int dealerValue, Hand dealerHand);
	void handleGameResolution(int dealerValue, double playerBet, Hand dealerHand);
	void handleLoseResolution();


public:
	void run();
	void playRound();
	BlackJack() {}
};



