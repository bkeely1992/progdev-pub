#include "Hand.h"

//Provided a card, adds it to the hand.
void Hand::addCardToHand(Card newCard)
{
	cards.insert(cards.begin(), newCard);
}

//Iterates through all of the cards in the hand to determine the blackjack value of the hand.
int Hand::getCurrentBlackjackValue()
{
	int currentHandValue = 0;
	int numberOfAces = 0;

	for (Card card : cards)
	{
		//Separate handling is required for aces because they can have either a 1 or 11 as their value.
		if (card.Value != 1)
		{
			//If the value is not an ace, you can take their value as it is and add it to the hand value.
			currentHandValue += card.Value;
		}
		else
		{
			numberOfAces++;
		}
	}

	//For each ace in the hand
	for (int i = 0; i < numberOfAces; i++)
	{
		//There can only be a max of one ace acting as 11, otherwise you immediately go bust.
		//And you only use the final ace as an 11 if it won't bust the hand.

		//This is handling for the final ace, to check if it'll bust the hand to take it as an 11.
		if (i == numberOfAces - 1)
		{
			if (currentHandValue <= 10)
			{
				currentHandValue += 11;
			}
			else
			{
				currentHandValue += 1;
			}
		}
		//All of the other aces in the hand will act as a 1.
		else
		{
			currentHandValue += 1;
		}
	}

	return currentHandValue;
}

//Clears the hand by removing all of the cards from it.
void Hand::clearHand()
{
	cards.clear();
}

//Returns a readable representation of the hand to be sent to the console.
string Hand::toString()
{
	string cardsRepresentation = "";

	for (Card card : cards)
	{
		cardsRepresentation += card.FullName + " | ";
	}

	return cardsRepresentation;
}

//Returns the number of cards in the hand.
int Hand::getSize() 
{
	return cards.size();
}