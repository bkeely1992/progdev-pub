#include "Player.h"
using namespace std;

//Given a card, adds it to the players hand and updates the hand's status in the context of BlackJack.
void Player::receiveCard(Card newCard)
{
	int currentValue = 0;
	hand.addCardToHand(newCard);

	currentValue = hand.getCurrentBlackjackValue();
	
	//If player's hand is above 21 then they are busted.
	if (currentValue > 21)
	{
		IsBusted = true;
	}
	//If player's hand is equal to 21 then they have a natural.
	else if (currentValue == 21 && hand.getSize() == 2)
	{
		HasNatural = true;
	}
}

//Clears the hand by calling the internal method of the player's hand.
void Player::clearCards()
{
	hand.clearHand();
}

//Returns a readable representation of the player's hand by accessing the internal method of the player's hand.
string Player::toString()
{
	return hand.toString();
}

//Returns the current blackjack value of the player's hand by accessing the internal method of the player's hand.
int Player::getCurrentBlackjackValue()
{
	return hand.getCurrentBlackjackValue();
}