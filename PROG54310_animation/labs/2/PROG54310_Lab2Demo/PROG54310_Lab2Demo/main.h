#pragma once
//Include GLEW
#include <GL/glew.h>

//Include GLFW
#include <GLFW/glfw3.h>

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

//
GLFWwindow* window;

int main(void) 
{
	//Continues executing only if the window is successfully created.
	if (!glfwInit()) 
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
		return -1;
	}

	//Anti-Aliasing -- 4x MSAA
	glfwWindowHint(GLFW_SAMPLES, 4);

	//Setting the major and minor versions
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	//Sets the profile
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	//Window size (in pixels), window name
	window = glfwCreateWindow(1024, 768, "First OpenGL Window", NULL, NULL);
	

	//This might happen if we're targetting a specific version, but the system doesn't support that specific versoin.
	if (window == NULL) 
	{
		fprintf(stderr, "Failed to open GLFW window. Check if your GPU supports OpenGL Version 3.3");
		glfwTerminate();
		return -1;
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	//Your main code stuffs go in here
	do 
	{
		/*START*/



		/*END*/


		//Swap buffer
		glfwSwapBuffers(window);
		glfwPollEvents();

	}//While escape key is not being pressed, continue code's execution AND the user has not attempted to close the window by clicking the X.
	while (glfwGetKey(window,GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0);


	
	glfwTerminate();

	return 0;
}