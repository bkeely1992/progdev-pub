-in the computer world we primarily care about 2-dimensional and 3-dimensional vectors
-a vector can be represented as a matrix.

Vector Addition
A+B = (x1,x2) + (y1,y2) = (x1+y1,x2+y2)

Scalar Multiplication
av = a(x1,x2) = (ax1,ax2)

Vector Dot Product
A*B = A^TB = [a b c][d  = ad + be + cf
					 e
					 f]

-think of the dot product as a matrix multiplication

-the magnitude is the dot product of a vector with itself
|A|^2 = A^TA = aa+bb+cc
-the dot product is also related to the angle between two vectors
A*B = |A||B|cos(angle)

Inner (dot) Product:
vw = (x1,x2)*(y1,y2) = x1y1 + x2y2

The inner product is a scalar value.

Basis: a space is totally defined by a set of vectors - any point is a linear combination of the basis.

-when using orthographic viewpoint, objects close to you and objects far away from you will appear to be
the same size [ie: there's no perspective where they get smaller as they get farther away]

Vectors: Cross Product
-the cross product of vectors A and B is a vector C which is perpendicular to A and Basis
-the magnitude of C is proportional to the sin of the angle between A and Basis
-the direction of C follows the riht hand rule if we are working in a right handed coordinate system

The dot product is important because it is the basis of almost all lighting calculations.
The dot product can be used to calculate the projection of one vector onto another.

How do we find the normal of a triangle?
How can we determine if a polygon is facing away from the camera?

We can multiply a matrix/vector to rotate the vector, translate the vector, scale the vector, etc..

