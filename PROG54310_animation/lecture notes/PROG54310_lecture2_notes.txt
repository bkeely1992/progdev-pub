nuget: remember to install nupengl.core

#include <GL/glew.h> ==> used for loading OpenGL extensions.
#include <GLFW/glfw3.h> ==> used for creating the window and handling the input.

The GLFWwindow object encapsulates both a window and a context. 
They are created with glfwCreateWindow and destroyed with glfwDestroyWindow. 
As the window and context are inseparably linked, the object pointer is used as both a context and widow handle.

glfwInit 
This functoin initializes the GLFW library. Before most GLFW functions can be used, GLFW must be initialized and before the program terminates, GLFW should be terminated in order to free any resources allocated during or after initialization.

glfwWindowHint ==> Changes values of a variety of settings for the window.

HINTS:
=====================
GLFW_SAMPLES ==> Sets the anti-aliasing value for the program.

GLFW_CONTEXT_VERSION_MAJOR and GLFW_CONTEXT_VERSION_MINOR refer to two components of a single version number.
Version 3.4 would be Major 3 and Minor 4

GLFW_OPENGL_PROFILE => sets the profile. Core is the default
=====================

glfwCreateWindow creates the window. Default dimensions are 1024, 768.

May want to check if the window is instantiated at this point. It might fail if your GPU doesn't support the targetted version of OpenGL.

glfwTerminate() => used to release the memory being used by OpenGL, freeing up those resources once you're done with using OpenGL for your application.
also will close the window if it's still open.

glfwSetInputMode() ==> this function sets an input mode option for the specified window.
If the mode is GLFW_STICKY_KEYS, the value must be either GLFW_TRUE to enable sticky keys or GLFW_FALSE to disable it. If sticky keys are enabled
a key press with ensure that glfwGetKey returns GLFW_PRESS the next time it is called even if the key have been released before the call.
This is useful when you are only interested in whether the keys have been pressed but not when or in which order.

glfwGetKey() ==> returns the status of that key if a change for that key is detected. ie( press, release, repeat)

glfwWindowShouldClose() ==> checks whether the user has closed the window.

glfwSwapBuffers() ==> this function swaps the front and back buffers of the specified window when rendering with OpenGL. If the swap interval is greater than zero,
the GPU driver waits the specified number of screen updates before swappin the buffers.

glfwPollEvents() ==> this function processes only those events that are already in the event queue and then returns immediately. Processing events will cause the window and input callbacks associated with
those events to be called. On some platforms, a window move, resize or menu operatoin will cause event processing to block. This is due to how event processing is designed on those platforms.
You can use the window refresh callback to redraw the contents of your window when necessary during such operations.
Do not assume that callbacks you set will only be called in response to event processing functions like this one. While it is necessary to poll for events, window systems that require GLFW to register callbacks of its own
can pass events to GLFW in response to many window system functoin calls. GLFW will pass those events on to the application callbacks before returning.

OpenGL Rendering Pipeline:
OpenGL implements what's commonly called a rendering pipeline. 

OpenGL begins with the geometric data you provide and first processes it through a sequence of shader stages.
The rasterizer will generate fragments for any primitive 

Preparing to send data to OpenGL
-OpenGL requires that all data be stored in buffer objects, which are just chunks of memory managed by the OpenGL server.
-Populating these buffers with data can occur in numerous ways, but one of the most common is using the glBufferData() command like in Triangle example.

-after we've initialized our buffers, we can request geometric primitives be rendered by calling one of OpenGL's drawing commands, such as glDrawArrays()

-drawing in openGL usually means transferring vertex data to the OpenGL server
-vertex is a bundle of data values that are processed tgether
-it almost always includes positional data and the pixel's final colour

MATH:

Local coordinate systems [sometimes called Object Space]
	-it's the coordinate system that the model was made in.
		
The World Space
	-it's the coordinate system of the world that the model will be used in.
	
-how did we get the monster positioned correctly in the world.

Camera Space
-everything relative to the positioning of the camera [and the camera never moves, the world moves around the camera]
-when the player moves forward in the game, mathematically speaking, everything in the world is moving relative to the camera while the camera stays stationary

how do we get from space to space?
	-have a model matrix
	-and a view matrix

-You can sometimes combine the model and view matrices together into a single matrix, but we won't be.



Linear Algebra - Vector Intro

A vector is a magnitude with a direction.
A measurement without a direction is called a scalar.
Adding a direction to the scalar will turn it into a vector.
IE: speed is a scalar, adding a direction to it will turn it into a velocity a vector.

If we represent a vector as an arrow, then its length will represent its magnitude
and the direction that the physical vector is pointing is the direction.

We can represent a vector mathematically is by putting an arrow on top of a variable.
It can also be represented by breaking down the directional magnitudes of the vector.
ie:
v = (5,0) or a = (3,2) [lines on top of letters aren't feasible in notepad++]

We can determine the magnitude of the vector by determining the slope:
ie: magnitude = (a^2+b^2)^0.5