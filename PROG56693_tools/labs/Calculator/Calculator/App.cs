﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Calculator
{
    class App
    {
        public void Run()
        {
            float inValueLeft = 0.0f;
            float inValueRight = 0.0f;
            float sumValue;
            string inputValue = "";

            //If the user enters in a value of q after an iteration of the calculator, then the calculator will exit out.
            do
            {

                //Prompts the user to enter in the first value to be summed.
                Console.WriteLine("Please enter in the first value to be summed:");

                //Stores it in a float by converting it from a string.
                inputValue = Console.ReadLine();

                //Exception handling in case the user inputs an incorrect value.
                try
                {
                    inValueLeft = System.Convert.ToSingle(inputValue);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Invalid input provided. Exiting.");
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    return;
                }

                //Prompts the user to enter in the second value to be summed.
                Console.WriteLine("Please enter in the second value to be summed:");
                //Stores it in a float by converting it from a string.
                inputValue = Console.ReadLine();

                //Exception handling in case the user inputs an incorrect value.
                try
                {
                    inValueRight = System.Convert.ToSingle(inputValue);
                }
                catch (Exception e)
                {
                    Console.WriteLine("Invalid input provided. Exiting.");
                    Console.WriteLine(e.Message);
                    Console.WriteLine(e.StackTrace);
                    return;
                }

                //Adds the values together
                sumValue = inValueLeft + inValueRight;

                //Writes the summation of the two values to the console.
                Console.WriteLine("The value of these two values summed together is {0}\n", sumValue);

                //Prompts user for if they want to exit out of the calculator.
                Console.WriteLine("Enter q to exit:");
                inputValue = Console.ReadLine();
            } while (inputValue != "q");
        }

    }
}
